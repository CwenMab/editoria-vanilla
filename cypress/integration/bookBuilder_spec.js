const checkComponentTypeToDivision = (division, el) => {
  cy.wrap(el.parent().next())
    .find(`[role="option"]`)
    .each(($el, index, $list) => {
      expect($el).to.contain(division.allowedComponentTypes[index].title)
    })
}

const addBookComponent = division =>
  cy
    .get(`[data-test-id="${division.name}-division"]`)
    .find('button')
    .first()
    .click({ force: true })

describe('BookBuilder', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/seedApplicationParameter.js')
    cy.exec('node ./scripts/createBooksWithUsersAndTeams.js')
  })

  it('production editor can navigate to book builder for the book she/he is assigned on', () => {
    cy.login('productionEditor')
    cy.getCollections().then(res => {
      const { body } = res
      const { data } = body
      const { getBookCollections } = data
      const collection = getBookCollections[0]
      const { books } = collection
      const productionEditorBook = books[0]

      cy.visit(`/books/${productionEditorBook.id}/book-builder`)
      cy.contains('FRONTMATTER')
      cy.contains('BODY')
      cy.contains('BACKMATTER')
    })
  })

  it('user can see the correct bookComponent Types to coresponding divisions', () => {
    cy.login('admin')

    cy.getApplicationParameters().then(parameters => {
      const {
        body: {
          data: { getApplicationParameters },
        },
      } = parameters

      const { config } = getApplicationParameters.find(
        parameter => parameter.area === 'divisions',
      )

      cy.getCollections().then(res => {
        const { body } = res
        const { data } = body
        const { getBookCollections } = data
        const collection = getBookCollections[0]
        const { books } = collection
        const productionEditorBook = books[0]

        cy.visit(`/books/${productionEditorBook.id}/book-builder`)

        config.map(division => {
          addBookComponent(division).then(() => {
            cy.get(`[data-test-id="${division.name}-division"]`)
              .find(`[data-test-id="component-types"]`)
              .click({ force: true })
              .then(el => {
                checkComponentTypeToDivision(division, el)
              })
          })
          return true
        })
      })
    })
  })

  it('user can add custom bookComponent Types to divisions', () => {
    cy.login('admin')

    cy.getApplicationParameters().then(parameters => {
      const {
        body: {
          data: { getApplicationParameters },
        },
      } = parameters

      const { config } = getApplicationParameters.find(
        parameter => parameter.area === 'divisions',
      )

      cy.getCollections().then(res => {
        const { body } = res
        const { data } = body
        const { getBookCollections } = data
        const collection = getBookCollections[0]
        const { books } = collection
        const productionEditorBook = books[0]

        cy.visit(`/books/${productionEditorBook.id}/book-builder`)

        config.map(division => {
          addBookComponent(division).then(() => {
            cy.get(`[data-test-id="${division.name}-division"]`)
              .find(`[data-test-id="component-types"]`)
              .click({ force: true })
              .then(el => {
                const elem = el.parent().next()
                cy.wrap(elem)
                  .find(`button`)
                  .click({ force: true })

                cy.wrap(elem)
                  .find('#addComponentType')
                  .type('test')

                cy.wrap(elem)
                  .find(`button`)
                  .click({ force: true })
              })
          })
          return true
        })

        cy.getApplicationParameters().then(parameter => {
          const {
            body: {
              data: { getApplicationParameters: applicationParameters },
            },
          } = parameter

          const { config: applicationConfig } = applicationParameters.find(
            param => param.area === 'divisions',
          )

          applicationConfig.map(division => {
            cy.get(`[data-test-id="${division.name}-division"]`)
              .find(`[data-test-id="component-types"]`)
              .then(el => {
                checkComponentTypeToDivision(division, el)
              })
            return true
          })
        })
      })
    })
  })
})
